function getData() {
    var feedURL = 'https://danielfernandezvega.medium.com/feed';
    let repos = [];
    let username = "danielfernandezvega";
    let githubUrl = `https://api.github.com/users/${username}/repos`;
    let token = "glpat-KF_5R8WnRNdKP-UysLvU";
    let gitlabUrl = `https://gitlab.com/api/v4/projects?owned=true&visibility=public&access_token=${token}`;

    fetch('https://api.allorigins.win/get?url=' + encodeURIComponent(feedURL))
        .then(response => response.json())
        .then(data => {
            var parser = new DOMParser();
            var xmlDoc = parser.parseFromString(data.contents, 'text/xml');
            var items = xmlDoc.querySelectorAll('item');
            var posts = [];
            for (let i = 0; i < 6; i++) {
                var item = `<div class="col">
                    <a target="_blank" class="text-decoration-none" href="${items[i].querySelector('link').textContent}">
                        <div class="card h-100 bg-transparent border-1 border-light text-light">
                            <div class="w-100 p-5 text-center h-100 bg-light bg-opacity-10">
                                <i class="fa-brands fa-medium fa-2xl"></i>
                                <p class="card-title mt-3">
                                    ${items[i].querySelector('title').textContent}
                                </p>
                            </div>
                        </div>
                    </a>
                </div>`
                posts.push(item);
            }
            posts = posts.join('');
            posts = posts.toString();
            document.getElementById('latest-post').innerHTML = posts;
        })
        .catch(error => console.error(error));


    // Realizar la solicitud GET a la API de GitHub
    fetch(githubUrl)
        .then(response => {
            // Verificar si la solicitud fue exitosa (código de estado 200)
            if (response.ok) {
                return response.json();
            }
            throw new Error('Hubo un problema al obtener los repositorios públicos.');
        })
        .then(data => {
            for (let i = 0; i < data.length; i++) {
                var repo = `<div class="col">
                    <a target="_blank" class="text-decoration-none" href="${data[i].html_url}">
                        <div class="card h-100 bg-transparent border-1 border-light text-light">
                            <div class="w-100 p-5 text-center bg-light bg-opacity-10">
                                <i class="fa-brands fa-github fa-2xl"></i>
                                <h5 class="card-title">${data[i].name}</h5>
                            </div>
                            <div class="card-body bg-light bg-opacity-25">
                                <p class="card-text">
                                ${data[i].description}
                                </p>
                            </div>
                        </div>
                    </a>
                </div>`
                repos.push(repo);
            }

            // Realizar la solicitud GET a la API de GitLab
            fetch(gitlabUrl)
                .then(response => {
                    // Verificar si la solicitud fue exitosa (código de estado 200)
                    if (response.ok) {
                        return response.json();
                    }
                    throw new Error('Hubo un problema al obtener los repositorios públicos.');
                })
                .then(data => {
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].name !== 'danielfernandezvega.gitlab.io') {
                            var repo = `<div class="col">
                    <a target="_blank" class="text-decoration-none" href="${data[i].web_url}">
                        <div class="card h-100 bg-transparent border-1 border-light text-light">
                            <div class="w-100 p-5 text-center bg-light bg-opacity-10">
                                <i class="fa-brands fa-gitlab fa-2xl"></i>
                                <h5 class="card-title">${data[i].name}</h5>
                            </div>
                            <div class="card-body bg-light bg-opacity-25">
                                <p class="card-text">
                                ${data[i].description}
                                </p>
                            </div>
                        </div>
                    </a>
                </div>`
                            repos = repos.concat(repo);
                        }
                    }

                    repos.sort(() => Math.random() - 0.5);
                    repos = repos.join('');
                    repos = repos.toString();
                    document.getElementById('projects').innerHTML = repos;

                })
                .catch(error => {
                    console.error(error.message);
                });
        })
        .catch(error => {
            console.error(error.message);
        });
}

window.onload = getData;