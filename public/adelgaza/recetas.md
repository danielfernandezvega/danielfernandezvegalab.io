## Jueves
Desayuno: Yogur griego con granola y frutas

Ingredientes:
1 taza de yogur griego sin azúcar añadido (aprox. 120 calorías)
1/4 taza de granola sin azúcar añadido (aprox. 100 calorías)
Frutas frescas picadas (como mango, piña, o kiwi) (varía según la cantidad y tipo de fruta)
Total aproximado de calorías: varía según las frutas utilizadas

Almuerzo: Tofu salteado con vegetales

Ingredientes:
Tofu firme cortado en cubos (aprox. 150 g, 180 calorías)
Vegetales salteados (pimientos, cebolla, champiñones, etc.) (aprox. 1 taza, 50 calorías)
Salsa de soja baja en sodio (aprox. 1 cucharada, 10 calorías)
Aceite de sésamo (aprox. 1 cucharadita, 40 calorías)
Total aproximado de calorías: alrededor de 280 calorías

Cena: Tofu salteado con brócoli y champiñones

Ingredientes:
Tofu firme cortado en cubos (aprox. 150 g, 180 calorías)
Brócoli al vapor (aprox. 1 taza, 55 calorías)
Champiñones en rodajas (aprox. 1/2 taza, 10 calorías)
Salsa de soja baja en sodio (aprox. 1 cucharada, 10 calorías)
Aceite de sésamo (aprox. 1 cucharadita, 40 calorías)
Total aproximado de calorías: alrededor de 295 calorías

Ejercicios:
Mañana: Rutina de ejercicios con banda elástica. Realiza ejercicios como estocadas con bandas, prensa de hombros, remo con banda y elevaciones frontales.
Tarde: 20 minutos de caminata en el parque. Esta vez, incluye algunos intervalos de carrera cortos si te sientes cómodo.

## Viernes
Desayuno: Panqueques de plátano

Ingredientes:
1 plátano maduro (aprox. 105 calorías)
2 huevos (aprox. 140 calorías)
1/4 cucharadita de canela en polvo (opcional) (aprox. 6 calorías)
Total aproximado de calorías: alrededor de 251 calorías

Almuerzo: Ensalada de atún y garbanzos

Ingredientes:
Atún enlatado en agua escurrido (aprox. 1 lata, 120 g, 130 calorías)
Garbanzos cocidos escurridos (aprox. 1/2 taza, 135 g, 135 calorías)
Pepino en cubitos (aprox. 1/2 taza, 8 calorías)
Cebolla roja picada (aprox. 2 cucharadas, 10 calorías)
Perejil fresco picado (para decorar, opcional)
Aderezo de vinagreta de limón sin azúcar añadido (aprox. 2 cucharadas, 60 calorías)
Total aproximado de calorías: alrededor de 343 calorías

Cena: Ensalada de garbanzos y vegetales asados

Ingredientes:
Garbanzos cocidos (aprox. 1 taza, 135 g, 135 calorías)
Vegetales para asar (calabaza, pimientos, cebolla, etc.) (aprox. 2 tazas, 50 calorías)
Pepino en rodajas (aprox. 1/2 taza, 8 calorías)
Cilantro fresco picado (para decorar, opcional)
Aderezo de limón sin azúcar añadido (aprox. 2 cucharadas, 60 calorías)
Total aproximado de calorías: alrededor de 253 calorías

Ejercicios:
Mañana: 30 minutos de bicicleta estática. Intenta aumentar la resistencia para desafiar un poco más tus músculos.
Tarde: 30 minutos de caminata en el parque. Haz una caminata vigorosa, manteniendo un ritmo constante.

## Sábado
Desayuno: Tazón de frutas con yogur

Ingredientes:
1 taza de yogur natural sin azúcar añadido (aprox. 120 calorías)
Frutas frescas picadas (como piña, melón, uvas, etc.) (varía según la cantidad y tipo de fruta)
Frutos secos picados (nueces, almendras, etc.) (varía según la cantidad)
Total aproximado de calorías: varía según las frutas y frutos secos utilizados

Almuerzo: Pimientos rellenos de quinoa y vegetales

Ingredientes:
Pimientos medianos (aprox. 2 unidades, 40 calorías)
Quinoa cocida (aprox. 1 taza, 185 calorías)
Mezcla de vegetales picados (calabacín, zanahoria, tomate, etc.) (aprox. 1 taza, 50 calorías)
Queso feta desmenuzado (opcional) (aprox. 2 cucharadas, 50 calorías)
Total aproximado de calorías: alrededor de 325 calorías

Cena: Pavo al horno con espárragos

Ingredientes:
Pechuga de pavo a la parrilla (aprox. 150 g, 170 calorías)
Espárragos frescos (aprox. 1 taza, 27 calorías)
Aceite de oliva (para rociar, aprox. 1 cucharadita, 40 calorías)
Tomillo fresco o seco (para espolvorear, opcional)
Total aproximado de calorías: alrededor de 237 calorías

Ejercicios:
Mañana: Entrenamiento de cuerpo completo con pesas. Realiza ejercicios como sentadillas con pesas, flexiones de piernas, press de pecho y elevaciones de hombros.
Tarde: Descanso activo. Realiza una caminata relajante por el parque durante 30-40 minutos para estirar los músculos y relajarte.

## Domingo
Desayuno: Huevos revueltos con vegetales

Ingredientes:
2 huevos (aprox. 140 calorías)
Vegetales picados (pimientos, cebolla, espinacas, champiñones, etc.) (varía según la cantidad y tipo de vegetales)
Total aproximado de calorías: varía según los vegetales utilizados

Almuerzo: Sopa de lentejas

Ingredientes:
Lentejas cocidas (aprox. 1 taza, 230 calorías)
Mezcla de vegetales picados (cebolla, zanahoria, apio, etc.) (aprox. 1 taza, 50 calorías)
Caldo de verduras bajo en sodio (aprox. 2 tazas, 20 calorías)
Especias al gusto (como pimienta, comino, etc.)
Total aproximado de calorías: alrededor de 300 calorías

Cena: Sopa de verduras

Ingredientes:
Caldo de verduras bajo en sodio (aprox. 2 tazas, 20 calorías)
Mezcla de vegetales picados (zanahoria, apio, cebolla, coliflor, etc.) (aprox. 2 tazas, 50 calorías)
Pechuga de pollo cocida y desmenuzada (aprox. 100 g, 110 calorías)
Hierbas frescas o secas al gusto (como perejil, albahaca, etc.)
Total aproximado de calorías: alrededor de 180 calorías

Ejercicios:
Mañana: 40 minutos de bicicleta estática. Alterna entre diferentes intensidades de pedaleo para desafiar tu resistencia.
Tarde: Descanso activo. Disfruta de una caminata suave por el parque durante 30 minutos para mantener la actividad física y relajarte antes de la semana siguiente.


## Snacks
Para la mañana:
Batido verde: Mezcla de espinacas, plátano, piña y leche de almendras sin azúcar añadido.
Yogur griego sin azúcar añadido con bayas frescas y una pizca de canela.
Puñado de almendras o nueces crudas.
Rodajas de manzana con mantequilla de almendras o nueces.
Mini tortillas de claras de huevo con espinacas y tomates cherry.

Para la tarde:
Palitos de zanahoria y apio con hummus casero.
Rodajas de pepino con queso cottage bajo en grasa y tomate cherry.
Batido de proteínas con leche de almendras, plátano y una cucharada de mantequilla de cacahuete.
Rodajas de pavo o jamón magro con rodajas de queso bajo en grasa.
Barrita de cereal casera hecha con avena, frutos secos y miel.

## Lista de supermercado
Frutas y Vegetales:
6 Plátanos
1 bolsa de Espinacas frescas (200g)
1 bandeja de Fresas (250g)
4 Manzanas
1 bandeja de Bayas frescas (250g)
2 Limones
2 Aguacates
1 paquete de Tomates cherry (250g)
1 Pepino
1 Calabacín
2 Pimientos (rojos, verdes)
1 Berenjena
4 Zanahorias
1 Calabaza (500g)
1 manojo de Espárragos
1 Cebolla
1 bandeja de Champiñones (200g)
1 Brócoli
3 dientes de Ajo
1 manojo de Perejil
1 manojo de Cilantro
1 Cebolla roja

Proteínas:
4 Pechugas de pollo (aprox. 600g)
2 filetes de Salmón fresco (aprox. 300g)
2 latas de Atún enlatado en agua
1 bloque de Tofu firme (aprox. 400g)
400g de Pavo
1 docena de Huevos
500g de Yogur griego natural sin azúcar añadido
250g de Queso cottage bajo en grasa
Queso feta desmenuzado (opcional)

Granos y Legumbres:
500g de Avena
250g de Quinoa
1 lata de Garbanzos enlatados
250g de Lentejas secas
Lácteos y Alternativas:
1L de Leche de almendras sin azúcar añadido
1 bote de Mantequilla de almendras o nueces
250g de Queso bajo en grasa
Hummus

Frutos Secos y Semillas:
200g de Almendras
200g de Nueces
100g de Semillas de chía
100g de Semillas de sésamo
100g de Semillas de girasol

Otros:
1 paquete de Pan integral sin azúcar añadido
500ml de Aceite de oliva
Vinagre balsámico
Vinagre de vino blanco
Especias variadas (canela, comino, pimienta, etc.)
Hierbas frescas variadas (tomillo, romero, albahaca, etc.)
Miel o jarabe de arce (opcional)
Barritas de cereal sin azúcar añadido (opcional)